import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestMutator {

    int NUM_OFFSPRING = 3;
    int DIMEN = 10;

    /**
     * Create a few offspring individuals for tests.
     * @return offspring: ArrayList of genotypes
     */
    ArrayList<Genotype> initialiseOffspring() {

        ArrayList<Genotype> offspring = new ArrayList<Genotype>();

        for (int i = 0; i < NUM_OFFSPRING; i++) {
            offspring.add(new Genotype(new double[10]));
        }

        return offspring;
    }

    @Test
    void testUniformMutate() {

        OptParameter params = new OptParameter();
        params.mutationStrategy = MutationStrategy.STATIC;
        params.mutationDistribution = MutationDistribution.UNIFORM;

        double [] oldGenes, newGenes;
        int rounds = 100000;
        int count = 0; // count the number of mutations

        ArrayList<Genotype> offspring = initialiseOffspring();
        //printGenes(offspring);

        for (int i = 0; i < rounds; i++) {

            oldGenes = offspring.get(0).genes.clone();
            offspring = Mutator.mutate(offspring, params);
            newGenes = offspring.get(0).genes.clone();

            for (int j = 0; j < DIMEN; j++) {

                if (newGenes[j] != oldGenes[j]) {
                    count++;
                }
            }
           // printGenes(offspring);
        }

        double prob_estimate = (double) count / (rounds * DIMEN);
        //System.out.println("Positionwise probability estimate: " + prob_estimate);

        // Check if the right amount of mutations took place
        assertTrue(prob_estimate > 0.024 && prob_estimate < 0.026);

        // Check that the same number of offspring is returned
        assertTrue(offspring.size() == NUM_OFFSPRING);
    }

    @Test
    void testGaussianMutate() {

        OptParameter params = new OptParameter();
        params.mutationStrategy = MutationStrategy.STATIC;
        params.mutationDistribution = MutationDistribution.GAUSSIAN;

        double [] oldGenes, newGenes;
        int rounds = 100000;
        int count = 0; // count the number of mutations

        ArrayList<Genotype> offspring = initialiseOffspring();

        //printGenes(offspring);

        for (int i = 0; i < rounds; i++) {

            oldGenes = offspring.get(0).genes.clone();
            offspring = Mutator.mutate(offspring, params);
            newGenes = offspring.get(0).genes.clone();

            for (int j = 0; j < DIMEN; j++) {

                if (newGenes[j] < oldGenes[j] + 0.25 && newGenes[j] > oldGenes[j] - 0.25) {
                    count++;
                }
            }
        }

        double prob_estimate = (double) count / (rounds * DIMEN);
        System.out.println("Positionwise probability estimate: " + prob_estimate);

        // Check if the right amount of mutations took place
        assertTrue(prob_estimate > 0.65 && prob_estimate < 0.7);

        // Check that the same number of offspring is returned
        assertTrue(offspring.size() == NUM_OFFSPRING);

    }

    @Test
    void testUncorrelatedOneStepMutate() {

        OptParameter params = new OptParameter();
        params.mutationStrategy = MutationStrategy.ONE_STEP;

        double [] oldGenes, newGenes;
        double newSigma;
        int rounds = 10000;
        int count = 0; // count the number of mutations

        ArrayList<Genotype> offspring = initialiseOffspring();

        //printGenes(offspring);

        for (int i = 0; i < rounds; i++) {

            //System.out.println("sigma " + offspring.get(0).sigma);

            offspring = initialiseOffspring();
            oldGenes = offspring.get(0).genes.clone();
            offspring = Mutator.mutate(offspring, params);
            newSigma = offspring.get(0).stepSize;
            newGenes = offspring.get(0).genes.clone();

            for (int j = 0; j < DIMEN; j++) {

                if (newGenes[j] <= oldGenes[j] + newSigma && newGenes[j] >= oldGenes[j] - newSigma) {
                    count++;
                }
            }
        }

        double prob_estimate = (double) count / (rounds * DIMEN);
        System.out.println("Positionwise probability estimate: " + prob_estimate);

        // Check if the right amount of mutations took place
        assertTrue(prob_estimate > 0.65 && prob_estimate < 0.70);

        rounds = 100;
        offspring = initialiseOffspring();
        count = 0;

        //printGenes(offspring);

        for (int i = 0; i < rounds; i++) {

            oldGenes = offspring.get(0).genes.clone();
            offspring = Mutator.mutate(offspring, params);
            newGenes = offspring.get(0).genes.clone();

            for (int j = 0; j < DIMEN; j++) {

                if (newGenes[j] < oldGenes[j]) {
                    count++;
                }
            }
        }

        prob_estimate = (double) count / (rounds * DIMEN);
        System.out.println("Positionwise probability estimate: " + prob_estimate);

        // Check if the right amount of mutations took place
        assertTrue(prob_estimate > 0.4 && prob_estimate < 0.6);

        // Check that the same number of offspring is returned
        assertTrue(offspring.size() == NUM_OFFSPRING);

    }

    @Test
    void testUncorrelatedNStepMutate() {

        OptParameter params = new OptParameter();
        params.mutationStrategy = MutationStrategy.N_STEP;

        double [] oldGenes, newGenes, newSigmas;
        int rounds = 10000;
        int count = 0; // count the number of mutations

        ArrayList<Genotype> offspring = initialiseOffspring();

        //printGenes(offspring);

        for (int i = 0; i < rounds; i++) {

            //System.out.println("sigma " + offspring.get(0).sigma);

            offspring = initialiseOffspring();
            oldGenes = offspring.get(0).genes.clone();
            offspring = Mutator.mutate(offspring, params);
            newSigmas = offspring.get(0).stepSizes.clone();
            newGenes = offspring.get(0).genes.clone();

            for (int j = 0; j < DIMEN; j++) {

                if (newGenes[j] < oldGenes[j] + newSigmas[j] && newGenes[j] > oldGenes[j] - newSigmas[j]) {
                    count++;
                }
            }
        }

        double prob_estimate = (double) count / (rounds * DIMEN);
        System.out.println("Positionwise probability estimate: " + prob_estimate);

        // Check if the right amount of mutations took place
        assertTrue(prob_estimate > 0.65 && prob_estimate < 0.75);

        rounds = 100;
        offspring = initialiseOffspring();
        count = 0;

        //printGenes(offspring);

        for (int i = 0; i < rounds; i++) {

            oldGenes = offspring.get(0).genes.clone();
            offspring = Mutator.mutate(offspring, params);
            newGenes = offspring.get(0).genes.clone();

            for (int j = 0; j < DIMEN; j++) {

                if (newGenes[j] < oldGenes[j]) {
                    count++;
                }
            }
        }

        prob_estimate = (double) count / (rounds * DIMEN);
        System.out.println("Positionwise probability estimate: " + prob_estimate);

        // Check if the right amount of mutations took place
        assertTrue(prob_estimate > 0.4 && prob_estimate < 0.6);

        // Check that the same number of offspring is returned
        assertTrue(offspring.size() == NUM_OFFSPRING);

    }
}