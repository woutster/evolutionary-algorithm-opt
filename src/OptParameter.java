public class OptParameter {

  public int populationSize = 100;

  public int iterations = 200;

  public int amountChildren = 1;

  public boolean mutate = true;

  // Recombinator selection parameters

  public double alpha = 0.5;

  public double crossoverPercentage = 1.0;

  public double crossoverPoint = 0.7;

  public RecombinationStrategy recombinationStrategy = RecombinationStrategy.BLENDED_X_OVER;

  public SurvivorSelectionStrategy survivorSelectionStrategy = SurvivorSelectionStrategy.REPLACEWORST;

  // Parent selection parameters

  public ParentSelectionStrategy parentSelectionStrategy = ParentSelectionStrategy.TOURNAMENT;

  public int numParents = 400;

  public int tournamentSize = 10;

  // Mutation parameters

  public MutationStrategy mutationStrategy = MutationStrategy.STATIC;

  public MutationDistribution mutationDistribution = MutationDistribution.GAUSSIAN;

  public double uniformMutationRate = 0.1;

  public double gaussianStaticStepSize = 0.1;

  public double gaussianInitialOneStepSize = 1.0;

  public double gaussianInitialNStepSize = 1.0;

  public double cauchyStaticStepSize = 0.05;

  public double cauchyInitialOneStepSize = 0.5;

  public double cauchyInitialNStepSize = 0.5;




}