import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestParentSelector {

    @Test
    void testSelectRank() {

        OptParameter props = new OptParameter();
        props.parentSelectionStrategy = ParentSelectionStrategy.RANK;
        props.populationSize = 3;

        ArrayList<Genotype> population = new ArrayList<Genotype>();
        population.add(new Genotype(new double[10], 0.1));
        population.add(new Genotype(new double[10], 0.3));
        population.add(new Genotype(new double[10], 0.2));

        ArrayList<Genotype> parents = ParentSelector.select(population, props);

        // Check that at least one parent was selected
        assertTrue(parents.size() > 0);

        // Check that the number of parents is less than the total population
        assertTrue(parents.size() < props.populationSize);
    }

}