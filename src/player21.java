import org.vu.contest.ContestEvaluation;
import org.vu.contest.ContestSubmission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;

public class player21 implements ContestSubmission
{
	Random rnd_;
	ContestEvaluation evaluation_;
    private int evaluations_limit_;
    private OptParameter params;
    private int evals;

	Population population = new Population();

	public player21()
	{
		rnd_ = new Random();
        params = new OptParameter();
	}

	public void setSeed(long seed)
	{
		// Set seed of algortihms random process
		rnd_.setSeed(seed);
	}

	public void setEvaluation(ContestEvaluation evaluation)
	{
		// Set evaluation problem used in the run
		evaluation_ = evaluation;

		// Get evaluation properties
		Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
		// Property keys depend on specific evaluation
		// E.g. double param = Double.parseDouble(props.getProperty("property_name"));
        boolean isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
        boolean hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
        boolean isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

		// Do sth with property values, e.g. specify relevant settings of your algorithm
        if(isMultimodal){
            // Do sth
        }else{
            // Do sth else

        }

        // recombinator = blendCrossover

    }

	public void run()
	{
		// Run your algorithm here

        evals = 0;

        // init population
        population.initialize(100, params);
        evaluateFitness(population.genotypes);

        // calculate fitness
        while(evals < evaluations_limit_){
            System.out.println("Current evals: " + evals );
        	// Evaluate current diversity
			double diversity = population.getDiversity();
			System.out.println("Current diversity: " + Double.toString(diversity));

			// Evaluate current best fitness
			Collections.sort(population.genotypes);
			double best_fitness = population.genotypes.get(0).fitness;
			System.out.println("Current best fitness: " + Double.toString(best_fitness));

            // Select parents
			ArrayList<Genotype> parents = ParentSelector.select(population.genotypes, params);

            // Apply crossover / mutation operators
			ArrayList<Genotype> offspring = Recombinator.blendCrossover(parents, params);

			// Add mutator operation here
			offspring = Mutator.mutate(offspring, params);

			// Evaluate offspring fitness
			evaluateFitness(offspring);

			// possibly create a termination function

            // Select survivors
			population.genotypes = SurvivorSelector.select(population.genotypes, offspring, params);
        }

	}

	public void evaluateFitness(ArrayList<Genotype> genotypes) {


		for (int i = 0; i < genotypes.size(); i++) {
			genotypes.get(i).fitness = (double) evaluation_.evaluate(genotypes.get(i).genes);
			evals++;

			if (evals >= evaluations_limit_) {
				break;
			}
		}

	}

	public static void main(String[] args) {
		player21 player = new player21();
		player.run();
	}








	
}
