public enum MutationDistribution {
    UNIFORM, GAUSSIAN, CAUCHY, LEVY
}
