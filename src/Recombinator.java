//There are three methods for recombination using floating point strings
//- discrete recombination
//- arithmetic recombination
//- blend recombination
//
//I'll be using the blend recombination since this is the most biological and best option according to the book
//Other options can ofcourse be explored later on

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Recombinator {


    private static Random parentIndex = new Random();

    public static ArrayList<Genotype> recombinate(ArrayList<Genotype> parents, OptParameter params) {

        ArrayList<Genotype> newPop = new ArrayList<Genotype>();

        if (params.recombinationStrategy == RecombinationStrategy.BLENDED_X_OVER) {
            newPop = blendCrossover(parents, params);
        }
        if (params.recombinationStrategy == RecombinationStrategy.SIMPLE_ARITHMETIC) {
            newPop = SimpleArithmeticRCrossover(parents, params);
        }
        if (params.recombinationStrategy == RecombinationStrategy.SINGLE_ARITHMETIC) {
            newPop = SingleArithmeticRCrossover(parents, params);
        }
        if (params.recombinationStrategy == RecombinationStrategy.WHOLE_ARITHMETIC) {
            newPop = WholeArithmeticRCrossover(parents, params);
        }

        return newPop;
    }


    // Creates a child child based on parents via Blended Crossover
    public static ArrayList<Genotype> blendCrossover(ArrayList<Genotype> parents, OptParameter params) {

        ArrayList<Genotype> newPop = new ArrayList<Genotype>();
        double[] childGenes;
        double[] childStepSizes;
        double amountCrossover = parents.size() * params.crossoverPercentage;
        Random u = new Random();
        double gamma;

        for (int i = 0; i < amountCrossover; i++) {
            Genotype parent1 = parents.get(parentIndex.nextInt((int) amountCrossover));
            Genotype parent2 = parents.get(parentIndex.nextInt((int) amountCrossover));

            childGenes = new double[10];
            childStepSizes = new double[10];
            for (int j = 0; j < params.amountChildren; j++) {


                gamma = (1 + 2 * params.alpha) * u.nextDouble() - params.alpha;
                for (int k = 0; k < 10; k++) {
                    childGenes[k] = (1 - gamma) * parent1.genes[k] + gamma * parent2.genes[k];
                    if (params.mutationStrategy == MutationStrategy.N_STEP) {
                        childStepSizes[k] = (1 - gamma) * parent1.stepSizes[k] + gamma * parent2.stepSizes[k];
                    }
                }


                if (params.mutationStrategy == MutationStrategy.ONE_STEP) {
                    double childStepSize = (1 - gamma) * parent1.stepSize + gamma * parent2.stepSize;
                    newPop.add(new Genotype(childGenes, childStepSize));
                } else if (params.mutationStrategy == MutationStrategy.N_STEP) {
                    newPop.add(new Genotype(childGenes, childStepSizes));
                } else {
                    newPop.add(new Genotype(childGenes, params));
                }
            }
        }

        return newPop;

    }

    // simple arithmetic crossover
    public static ArrayList<Genotype> SimpleArithmeticRCrossover(ArrayList<Genotype> parents, OptParameter params) {

        ArrayList<Genotype> newPop = new ArrayList<Genotype>();
        double[] child1;
        double[] child2;
        double amountCrossover = parents.size() * params.crossoverPercentage;

        for (int i = 0; i < amountCrossover; i++) {
            Genotype parent1 = parents.get(parentIndex.nextInt((int) amountCrossover));
            Genotype parent2 = parents.get(parentIndex.nextInt((int) amountCrossover));

            child1 = new double[10];
            child2 = new double[10];

            for (int j = 0; j < 10; j++) {
                if (j < params.crossoverPoint) {
                    child1[j] = parent1.genes[j];
                    child2[j] = parent2.genes[j];
                } else {
                    child1[j] = params.alpha * parent1.genes[j] + (1 - params.alpha) * parent2.genes[j];
                    child2[j] = params.alpha * parent2.genes[j] + (1 - params.alpha) * parent1.genes[j];
                }

            }
            newPop.add(new Genotype(child1, params));
            newPop.add(new Genotype(child2, params));
        }
        return newPop;
    }

    // single arithmetic crossover
    public static ArrayList<Genotype> SingleArithmeticRCrossover(ArrayList<Genotype> parents, OptParameter params) {

        ArrayList<Genotype> newPop = new ArrayList<Genotype>();
        double[] child1;
        double[] child2;
        double amountCrossover = parents.size() * params.crossoverPercentage;

        for (int i = 0; i < amountCrossover; i++) {
            Genotype parent1 = parents.get(parentIndex.nextInt((int) amountCrossover));
            Genotype parent2 = parents.get(parentIndex.nextInt((int) amountCrossover));

            child1 = new double[10];
            child2 = new double[10];

            for (int j = 0; j < 10; j++) {
                if (j == params.crossoverPoint) {
                    double parentMean = params.alpha * parent1.genes[j] + (1 - params.alpha) * parent2.genes[j];
                    child1[j] = parentMean;
                    child2[j] = parentMean;
                } else {

                    child1[j] = parent1.genes[j];
                    child2[j] = parent2.genes[j];
                }

            }
            newPop.add(new Genotype(child1, params));
            newPop.add(new Genotype(child2, params));
        }
        return newPop;
    }

    // whole arithmetic crossover
    public static ArrayList<Genotype> WholeArithmeticRCrossover(ArrayList<Genotype> parents, OptParameter params) {

        ArrayList<Genotype> newPop = new ArrayList<Genotype>();
        double[] child1;
        double[] child2;
        double amountCrossover = parents.size() * params.crossoverPercentage;

        for (int i = 0; i < amountCrossover; i++) {
            Genotype parent1 = parents.get(parentIndex.nextInt((int) amountCrossover));
            Genotype parent2 = parents.get(parentIndex.nextInt((int) amountCrossover));

            child1 = new double[10];
            child2 = new double[10];

            for (int j = 0; j < 10; j++) {

                child1[j] = params.alpha * parent1.genes[j] + (1 - params.alpha) * parent2.genes[j];
                child2[j] = params.alpha * parent2.genes[j] + (1 - params.alpha) * parent1.genes[j];


            }
            newPop.add(new Genotype(child1, params));
            newPop.add(new Genotype(child2, params));
        }
        return newPop;
    }

}
