import java.util.ArrayList;
import java.util.Random;

public class Population {

    public ArrayList<Genotype> genotypes = new ArrayList<Genotype>();
    private int genotypeSize = 10;
    private int upperLimit = 5;
    private int lowerLimit = -5;
    private Random rnd = new Random();

    public void initialize(int populationSize, OptParameter params) {
        for (int row = 0; row < populationSize; row++) {
            genotypes.add(new Genotype(generateGenes(), params));
        }
    }

    private double[] generateGenes() {
        double[] genes = new double[genotypeSize];
        for (int i = 0; i < genotypeSize; i++) {
            genes[i] = rnd.nextDouble() * (upperLimit - lowerLimit) + lowerLimit;
        }
        return genes;
    }

    public double getDiversity() {
        // Create matrix that combines all genes from the given population
        double[][] geneMatrix = new double[genotypeSize][this.genotypes.size()];
        for (int i = 0; i < genotypeSize; i++) {
            for (int j = 0; j < this.genotypes.size(); j++) {
                geneMatrix[i][j] = this.genotypes.get(j).genes[i];
            }
        }
        // Calculate a vector that contains the variance per gene column
        double[] variance = new double[genotypeSize];
        for (int i = 0; i < genotypeSize; i++) {
            variance[i] = getVariance(geneMatrix[i]);
        }

        // Calculate the mean over the variance vector
        return getMean(variance);
    }

    private double getVariance(double[] data) {
        double mean = getMean(data);
        int size = data.length;
        double temp = 0;
        for(double a :data)
            temp += (a-mean)*(a-mean);
        return temp/(size-1);
    }

    private double getMean(double[] data) {
        double sum = 0.0;
        int size = data.length;
        for(double a : data)
            sum += a;
        return sum/size;
    }

}
