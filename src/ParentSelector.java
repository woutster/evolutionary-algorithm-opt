
/**
 * Selects the parents of the next generation from the current population. There are various
 * strategies that can be applied, some of which distinguish among individuals based on their 
 * quality in order to allow the better individuals to become parents of the next generation.
 * Some of the possible strategies include Fitness-Proportionate Selection, Rank-based Selection,
 * Tournament Selection, and Uniform Selection.
 * (see https://www.uio.no/studier/emner/matnat/ifi/INF3490/h15/lectures/lecture3-inf3490-evolalg2-ho-1pp.pdf)
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ParentSelector {

    /**
     * @return The new parents of the next generation.
     */
    public static ArrayList<Genotype> select(ArrayList<Genotype> population, OptParameter parameter) {

        ArrayList<Genotype> parents = new ArrayList<Genotype>();

        if (parameter.parentSelectionStrategy == ParentSelectionStrategy.RANK) {
            // Repeat parent selection if none chosen
            do {
                parents =  rankSelect(population, parameter);
            } while (parents.size() == 0);
        }
        if (parameter.parentSelectionStrategy == ParentSelectionStrategy.ROULETTE) {
            // Repeat parent selection if none chosen
            do {
                parents =  rouletteSelect(population, parameter);
            } while (parents.size() == 0);
        }
        if (parameter.parentSelectionStrategy == ParentSelectionStrategy.TOURNAMENT) {
             //Repeat parent selection if none chosen
             do {
                 parents =  tournamentSelect(population, parameter);
             } while (parents.size() == 0);
        }


        return parents;
    }

    /**
     * @return The new parents of the next generation.
     */
    public static ArrayList<Genotype> rankSelect(ArrayList<Genotype> population, OptParameter parameter) {

    	Collections.sort(population);

    	// Calculate rank-based probabilities for each individual
        double [] probs = new double[parameter.populationSize];

        for (int i = 0; i < parameter.populationSize; i++) {
            probs[i] = (double) (parameter.populationSize - i - 1) / parameter.populationSize;
        }

    	// Generate random numbers in range [0, 1) for every individual in the population.
        Random random = new Random();
        ArrayList<Genotype> parents = new ArrayList<Genotype>();

        for (int i = 0; i < parameter.populationSize; i++) {

            if (probs[i] >= random.nextFloat()) {
                parents.add(population.get(i));
            }
        }

    	return parents;
    }

   /**
     * @return The new parents of the next generation.
     */
    public static ArrayList<Genotype> rouletteSelect(ArrayList<Genotype> population, OptParameter parameter) {
        
        ArrayList<Genotype> parents = new ArrayList<Genotype>();
        int sum = 0;

        for(int i = parameter.populationSize-1; i >= 0; i--) {

            sum += population.get(i).fitness;
        }

        Random random = new Random();
        double r = 0 + (sum - 0) * random.nextDouble();

        int partialSum = 0;

        for(int i = parameter.populationSize-1; i >= 0; i--) {

            partialSum += population.get(i).fitness;

            if (partialSum >= r) {
                parents.add(population.get(i));
            }
        }
        return parents;
    }

    /**
     * The method works by holding pairwise tournament competitions in round-robin format, where each individual
     * is evaluated against q others randomly chosen from the merged parent and offspring populations.
     *
     * @return The population of genotypes after the selection
     */
    private static ArrayList<Genotype> tournamentSelect(ArrayList<Genotype> population, OptParameter parameter) {

        ArrayList<Genotype> new_gen = new ArrayList<Genotype>();
        Random random = new Random();

        while (new_gen.size() < parameter.numParents) {

            // Choose k participants for tournament
            ArrayList<Genotype> participants = new ArrayList<Genotype>();
            for (int j = 0; j < parameter.tournamentSize; j++) {
                int index = random.nextInt(population.size());
                participants.add(population.get(index));
            }

            // Sort them by fitness
            Collections.sort(participants);

            // Select strongest individual
            new_gen.add(participants.get(parameter.tournamentSize-1));
        }
//        System.out.print(new_gen.size());
        return new_gen;
    }

}
