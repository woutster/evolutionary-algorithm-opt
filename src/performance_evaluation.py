import subprocess
import re
import csv
import os
import matplotlib.pyplot as plt

# Paramter that configure the performance evaluation
TOTAL_RUNS = 100  # If this is more than 1, the average of all runs will be taken
EVAL_FUNCTION = "BentCigarFunction"  # BentCigarFunction, KatsuuraEvaluation, SchaffersEvaluation
FILE_NAME = "BentCigar1"  # Name of file that stores the results of this run
INCLUDE_IN_PLOT = ["BentCigar1"]  # Filenames of old runs that should be included in plot
SUBFOLDER = "plot_data"


def read_from_file(filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        lines = list(reader)
        evals = [int(i) for i in lines[0]]
        fitness = [float(i) for i in lines[1]]
        diversity = [float(i) for i in lines[2]]
    return evals, fitness, diversity


# Compile java files
cmd = "javac -cp contest.jar player21.java Population.java OptParameter.java Genotype.java ParentSelector.java SurvivorSelector.java ParentSelectionStrategy.java SurvivorSelectionStrategy.java Recombinator.java RecombinationStrategy.java Mutator.java MutationStrategy.java MutationDistribution.java"
subprocess.run(cmd.split(sep=' '))
cmd = "jar cmf MainClass.txt submission.jar player21.class Population.class OptParameter.class Genotype.class ParentSelector.class SurvivorSelector.class ParentSelectionStrategy.class SurvivorSelectionStrategy.class Recombinator.class RecombinationStrategy.class Mutator.class MutationStrategy.class SurvivorSelector$1.class MutationDistribution.class"
subprocess.run(cmd.split(sep=' '))

lst_evals = []
lst_fitness = []
lst_diversity = []
for i in range(TOTAL_RUNS):
    cmd = "java -jar testrun.jar -submission=player21 -evaluation={} -seed=1".format(EVAL_FUNCTION)
    result = subprocess.run(cmd.split(sep=' '), stdout=subprocess.PIPE)
    cmd_text = result.stdout.decode().split(sep='\n')
    j = 0
    lst_ind = 0
    while j < len(cmd_text) - 3:
        evals = int(re.findall(pattern=r'-?\d+\.?\d*', string=cmd_text[j])[0])
        diversity = float(re.findall(pattern=r'-?\d+\.?\d*', string=cmd_text[j + 1])[0])
        fitness = float(re.findall(pattern=r'-?\d+\.?\d*', string=cmd_text[j + 2])[0])
        # Add values to list during first run, otherwise update the current mean
        if i == 0:
            lst_evals.append(evals)
            lst_fitness.append(fitness)
            lst_diversity.append(diversity)
        else:
            lst_fitness[lst_ind] = (lst_ind * lst_fitness[lst_ind] + fitness) / (lst_ind + 1)
            lst_diversity[lst_ind] = (lst_ind * lst_diversity[lst_ind] + diversity) / (lst_ind + 1)
        j += 3
        lst_ind += 1

# Save values of this run to file
with open(os.path.join(SUBFOLDER, FILE_NAME), "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerow(lst_evals)
    writer.writerow(lst_fitness)
    writer.writerow(lst_diversity)

# Start plotting
plt.style.use('ggplot')
fig = plt.figure()
ax = plt.subplot(111)

# Add plots from saved files
for f in INCLUDE_IN_PLOT:
    evals, fitness, diversity = read_from_file(os.path.join(SUBFOLDER, f))
    ax.plot(evals, diversity, label='Diversity ({})'.format(f))
    ax.plot(evals, fitness, label='Fitness ({})'.format(f))

# Show plots
plt.xlabel("Evaluations")
ax.legend()
plt.show()
