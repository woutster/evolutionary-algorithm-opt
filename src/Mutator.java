/*
   each individual in the population is a collection of doubles so each double represents
   an individual. I used the gaussian mutation method in this class which does the following:
   A random gaussian distribution value with a mean of 0.0
   is added to the current individual. This value is the mutated individual if it is in the 
   range of [lower, upper] otherwise it will be changed to the closest value in the range
   meaning it is changed to either lower or upper.

*/

import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;
//import org.apache.commons.math3.distribution.MultivariateNormalDistribution;

public class Mutator {

    private static double LOWER = -5.0;
    private static double UPPER = 5.0;
    private static Random random = new Random();

    /**
     * @return The newly mutated offspring.
     */
    public static ArrayList<Genotype> mutate(ArrayList<Genotype> offspring, OptParameter params) {

        if (params.mutationStrategy == MutationStrategy.STATIC) {
            offspring = staticMutate(offspring, params);
        } else if (params.mutationStrategy == MutationStrategy.ONE_STEP) {
            offspring = oneStepMutate(offspring, params);
        } else if (params.mutationStrategy == MutationStrategy.N_STEP) {
            offspring = uncorrelatedNStepMutate(offspring, params);
        }

        return offspring;
    }

    /**
     * @return The newly uniformly mutated offspring.
     */
    public static ArrayList<Genotype> staticMutate(ArrayList<Genotype> offspring, OptParameter params) {

        if (params.mutationDistribution == MutationDistribution.UNIFORM)
            offspring = uniformMutate(offspring, params);
        else
            offspring = nonUniformMutate(offspring, params);

        return offspring;
    }

    /**
     * @return The newly uniformly mutated offspring.
     */
    public static ArrayList<Genotype> uniformMutate(ArrayList<Genotype> offspring, OptParameter params) {

        double positionSample;
        double uniformSample;

        for (Genotype child : offspring) {

            for (int i = 0; i < child.genes.length; i++) {

                positionSample = random.nextDouble();

                if (positionSample < params.uniformMutationRate) {
                    uniformSample = LOWER + (UPPER - LOWER) * random.nextDouble();
                    child.genes[i] = uniformSample;
                }
            }
        }

        return offspring;
    }

    /**
     * @return The newly Gaussian mutated offspring.
     */
    public static ArrayList<Genotype> nonUniformMutate(ArrayList<Genotype> offspring, OptParameter params) {

        double sample;
        double newGene;
        double staticStepSize;

        if (params.mutationDistribution == MutationDistribution.GAUSSIAN)
            staticStepSize = params.gaussianStaticStepSize;
        else
            staticStepSize = params.cauchyStaticStepSize;

        for (Genotype child : offspring) {

            for (int i = 0; i < child.genes.length; i++) {
                sample = newSample(staticStepSize, params);
                newGene = child.genes[i] + sample;
                newGene = Math.min(Math.max(newGene, LOWER), UPPER);
                child.genes[i] = newGene;
            }
        }

        return offspring;
    }

    // Parameters used for self-adapting mutation strategies
    private static int n = 10; // problem size
    private static double threshold = 0.01; // threshold above which to hold the step size

    /**
     * @return The newly mutated offspring.
     */
    public static ArrayList<Genotype> oneStepMutate(ArrayList<Genotype> offspring, OptParameter params) {

        double tau = 1 / Math.sqrt(n); // tau is usually inversely proportional to the square root of the problem size

        double lognormalSample;
        double sample;
        double newSigma;
        double newGene;

        for (Genotype child : offspring) {

            lognormalSample = Math.exp(tau * random.nextGaussian());
            newSigma = child.stepSize * lognormalSample;
            newSigma = Math.max(threshold, newSigma);
            child.stepSize = newSigma;

            for (int i = 0; i < child.genes.length; i++) {

                sample = newSample(child.stepSize, params);
                newGene = child.genes[i] + sample;
                newGene = Math.min(Math.max(newGene, LOWER), UPPER);
                child.genes[i] = newGene;
            }
        }

        return offspring;
    }

    /**
     * @return The newly Gaussian mutated offspring.
     */
    public static ArrayList<Genotype> uncorrelatedNStepMutate(ArrayList<Genotype> offspring, OptParameter params) {

        double tau = 1 / Math.sqrt(2 * Math.sqrt(n)); // tau is usually inversely proportional to 1/sqrt(2n)
        double tauPrime = 1 / Math.sqrt(2 * n); // tau prime is usually inversly proportional to 1/sqrt(2sqrt(n))

        double commonLognormalSample;
        double lognormalSample;
        double sample;
        double newSigma;
        double newGene;

        for (Genotype child : offspring) {

            commonLognormalSample = Math.exp(tauPrime * random.nextGaussian());

            for (int i = 0; i < child.genes.length; i++) {
                lognormalSample = Math.exp(tau * random.nextGaussian());
                newSigma = child.stepSizes[i] * lognormalSample * commonLognormalSample;
                newSigma = Math.max(threshold, newSigma);
                child.stepSizes[i] = newSigma;

                sample = newSample(child.stepSizes[i], params);
                newGene = child.genes[i] + sample;
                newGene = Math.min(Math.max(newGene, LOWER), UPPER);
                child.genes[i] = newGene;
            }
        }

        return offspring;
    }

    public static double newSample(double stepSize, OptParameter params) {

        double sample;

        if (params.mutationDistribution == MutationDistribution.GAUSSIAN)
            sample = random.nextGaussian() * stepSize;
        else // if (params.mutationDistribution == MutationDistribution.CAUCHY)
            sample = stepSize * Math.tan(Math.PI * (random.nextDouble() - 0.5));

        return sample;
    }
}
