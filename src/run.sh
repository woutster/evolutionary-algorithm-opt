javac -cp contest.jar player21.java Population.java OptParameter.java Genotype.java ParentSelector.java SurvivorSelector.java ParentSelectionStrategy.java SurvivorSelectionStrategy.java Recombinator.java RecombinationStrategy.java Mutator.java MutationStrategy.java MutationDistribution.java
jar cmf MainClass.txt submission.jar player21.class Population.class OptParameter.class Genotype.class ParentSelector.class SurvivorSelector.class  ParentSelectionStrategy.class SurvivorSelectionStrategy.class Recombinator.class RecombinationStrategy.class Mutator.class MutationStrategy.class SurvivorSelector\$1.class MutationDistribution.class
java -jar testrun.jar -submission=player21 -evaluation=BentCigarFunction -seed=1


#KatsuuraEvaluation
#BentCigarFunction
#SchaffersEvaluation

#export LD_LIBRARY_PATH=~/Dropbox/School/Evolutionary\ computing/evolutionary-algorithm-opt/src/
