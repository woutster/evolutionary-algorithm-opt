public enum MutationStrategy {
    STATIC, ONE_STEP, N_STEP
}
