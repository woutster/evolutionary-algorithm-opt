import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class PopulationTest {

    @Test
    void initialize() {
        Population pop1 = new Population();
        Population pop2 = new Population();

        pop1.initialize(100);
        pop2.initialize(100);

        System.out.println(pop1.genotypes.size());

        System.out.println(pop2.genotypes.get(0).genes[0]);
    }

    @Test
    void testDiversity() {
        Population population = new Population();
        population.initialize(100);

        double diversity = population.getDiversity();

        assertTrue(diversity != 0);
    }
}