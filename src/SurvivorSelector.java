import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Distinguishes among individuals based on their quality. It reduces the population
 * of x parents and i offsprings to y individuals forming the new generation.
 * The two major strategies are "Age-based Replacement" and "Fitness-based Replacement".
 * The survivor selection is called after the creation of the
 * offspring from the selected parents.
 */
public class SurvivorSelector {

    /**
     * @return The new population after parent selection
     */
    public static ArrayList<Genotype> select(ArrayList<Genotype> parents,
                                             ArrayList<Genotype> offspring,
                                             OptParameter params) {

        if (parents == null) {
            throw new IllegalArgumentException("Parents cannot be null!");
        }

        if (parents.size() != params.populationSize) {
            throw new IllegalArgumentException("The parent population does not match the parameter!");
        }

        if (offspring == null) {
            throw new IllegalArgumentException("Offspring cannot be null!");
        }

        switch (params.survivorSelectionStrategy) {
            case REPLACEWORST:
                return selectByReplaceWorst(parents, offspring);
            case MU_LAMBDA:
                return selectMuLambda(offspring, params);
            case MU_PLUS_LAMBDA:
                return selectByMuPlusLamba(parents, offspring, params);
            case AGE:
                return selectByAge(offspring, params);
            case ROUND_ROBIN:
                return selectByRoundRobin(parents, offspring, params);
            default:
                throw new IllegalArgumentException("The specified survivor selection strategy is not supported.");
        }
    }


    /**
     * The worst members of the parent population are selected for replacement
     *
     * @return The population of genotypes after the selection
     */
    private static ArrayList<Genotype> selectByReplaceWorst(ArrayList<Genotype> parents,
                                                            ArrayList<Genotype> offspring) {
        // Sort list of genotypes by their fitness
        Collections.sort(parents);
        Collections.sort(offspring);

        if (parents.size() == offspring.size()) {
            return offspring;

        }
        if (parents.size() > offspring.size()) {
            // Remove the parents with the lowest fitness value and replace them with the offspring
            parents.subList(0, offspring.size()).clear();
            parents.addAll(offspring);
            return parents;

        } else {
            offspring.subList(0, offspring.size() - parents.size()).clear();
            return offspring;
        }
    }


    /**
     * Selects the fittest individuals of the offspring while ignoring all parents
     *
     * @return The population of genotypes after the selection
     */
    private static ArrayList<Genotype> selectMuLambda(ArrayList<Genotype> offspring,
                                                      OptParameter parameter) {

        if (offspring.size() < parameter.populationSize)
            throw new IllegalArgumentException("For Mu-plus-lambda selection, the number of individuals in the" +
                    " offspring must be greater than the population size");

        Collections.sort(offspring);
        offspring.subList(0, parameter.populationSize).clear();
        return offspring;
    }


    /**
     * Combines the offspring and parent individuals and selects the fittest individuals
     * In Evolution Strategies λ > μ with a great offspring surplus (typically λ/μ ≈ 5 − 7)
     * that induces a large selection pressure.
     *
     * @return The population of genotypes after the selection
     */
    private static ArrayList<Genotype> selectByMuPlusLamba(ArrayList<Genotype> parents,
                                                           ArrayList<Genotype> offspring,
                                                           OptParameter parameter) {
        offspring.addAll(parents);
        Collections.sort(offspring);
        offspring.subList(0, parameter.populationSize).clear();
        return offspring;

    }


    /**
     * Replaces the current population with the offspring
     *
     * @return The population of genotypes after the selection
     */
    private static ArrayList<Genotype> selectByAge(ArrayList<Genotype> offspring,
                                                   OptParameter parameter) {

        if (offspring.size() != parameter.populationSize)
            throw new IllegalArgumentException("For age-based selection, the number of offspring individuals " +
                    "must match the population size.");

        return offspring;

    }

    /**
     * The method works by holding pairwise tournament competitions in round-robin format, where each individual
     * is evaluated against q others randomly chosen from the merged parent and offspring populations.
     *
     * @return The population of genotypes after the selection
     */
    private static ArrayList<Genotype> selectByRoundRobin(ArrayList<Genotype> parents,
                                                          ArrayList<Genotype> offspring,
                                                          OptParameter parameter) {
        // q is the number of opponents each individual has to win against to survive
        final int q = 10;
        // Merge offspring and parents
        offspring.addAll(parents);
        Random rnd = new Random();
        ArrayList<Genotype> new_gen = new ArrayList<Genotype>();

        while (new_gen.size() < parameter.populationSize) {
            // loop over all individuals in population
            for (int i=0; i<offspring.size(); i++) {
                Genotype current_individual = offspring.get(i);

                // For each individual, randomly choose q opponents to "fight" against
                for (int j=0; j<q; j++) {
                    int index = rnd.nextInt(offspring.size());
                    Genotype oponent = offspring.get(index);
                    if (current_individual.fitness>oponent.fitness) {
                        new_gen.add(current_individual);
                        break;
                    }
                }

                if (new_gen.size() == parameter.populationSize)
                    break;
            }
        }

        return new_gen;
    }

}