public enum SurvivorSelectionStrategy {
    AGE, MU_LAMBDA, MU_PLUS_LAMBDA, REPLACEWORST, ROUND_ROBIN, ELITISM
}