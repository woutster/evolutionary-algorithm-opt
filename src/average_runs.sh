javac -cp contest.jar player21.java Population.java OptParameter.java Genotype.java ParentSelector.java SurvivorSelector.java ParentSelectionStrategy.java SurvivorSelectionStrategy.java Recombinator.java RecombinationStrategy.java Mutator.java MutationStrategy.java MutationDistribution.java
jar cmf MainClass.txt submission.jar player21.class Population.class OptParameter.class Genotype.class ParentSelector.class SurvivorSelector.class  ParentSelectionStrategy.class SurvivorSelectionStrategy.class Recombinator.class RecombinationStrategy.class Mutator.class MutationStrategy.class SurvivorSelector\$1.class MutationDistribution.class
total=0.0
runs=30
for (( i = 0; i < $runs; i++))
do 
    OUTPUT=$(java -jar testrun.jar -submission=player21 -evaluation=BentCigarFunction -seed=1)
    SCORE=$(echo $OUTPUT | perl -nle 'm/Score: ([^ ]*)/; print $1') 
    total=$(echo "$total + $SCORE" | bc)
    #echo $SCORE
done
average=$(echo "$total / $runs" | bc -l)
echo $average
# BentCigarFunction
# SchaffersEvaluation
# KatsuuraEvaluation
