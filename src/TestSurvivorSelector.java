import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestSurvivorSelector {

    @Test
    void testSelectByReplaceWorst() {

        OptParameter props = new OptParameter();
        props.survivorSelectionStrategy = SurvivorSelectionStrategy.REPLACEWORST;
        props.populationSize = 3;

        ArrayList<Genotype> parents = new ArrayList<Genotype>();
        parents.add(new Genotype(new double[10], 0.1));
        parents.add(new Genotype(new double[10], 0.3));
        parents.add(new Genotype(new double[10], 0.2));

        ArrayList<Genotype> offspring = new ArrayList<Genotype>();
        offspring.add(new Genotype(new double[10], 0.5));
        offspring.add(new Genotype(new double[10], 0.7));


        ArrayList<Genotype> newPopulation = SurvivorSelector.select(parents, offspring, props);
        // Check if the population size remains constant
        assertEquals(props.populationSize, newPopulation.size());
        // Check if the survivor selection was correct
        assertTrue(newPopulation.get(0).fitness > 0.2);
        assertTrue(newPopulation.get(1).fitness > 0.2);
        assertTrue(newPopulation.get(2).fitness > 0.2);
    }


    @Test
    void testSelectByRoundRobin() {
        OptParameter props = new OptParameter();
        props.survivorSelectionStrategy = SurvivorSelectionStrategy.ROUND_ROBIN;
        props.populationSize = 5;

        ArrayList<Genotype> parents = new ArrayList<Genotype>();
        parents.add(new Genotype(new double[10], 0.1));
        parents.add(new Genotype(new double[10], 0.3));
        parents.add(new Genotype(new double[10], 0.2));
        parents.add(new Genotype(new double[10], 0.2));
        parents.add(new Genotype(new double[10], 0.2));

        ArrayList<Genotype> offspring = new ArrayList<Genotype>();
        offspring.add(new Genotype(new double[10], 0.5));
        offspring.add(new Genotype(new double[10], 0.7));

        ArrayList<Genotype> newPopulation = SurvivorSelector.select(parents, offspring, props);
        // Check if the population size remains constant
        assertEquals(props.populationSize, newPopulation.size());
        // Check if the survivor selection was correct
        assertTrue(newPopulation.get(0).fitness > 0.2);
        assertTrue(newPopulation.get(1).fitness > 0.2);
        assertTrue(newPopulation.get(2).fitness > 0.2);
    }
}