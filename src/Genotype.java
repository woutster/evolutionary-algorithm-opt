public class Genotype implements Comparable<Genotype> {
    public double[] genes;
    public double stepSize;
    public double[] stepSizes;
    public double fitness;
    public int age;

    public Genotype(double[] genes, OptParameter params) {
        this.genes = genes;

        // Ideally only initialise these variables when the corresponding mutation strategy is used

        if (params.mutationStrategy == MutationStrategy.ONE_STEP) {
            if (params.mutationDistribution == MutationDistribution.GAUSSIAN)
                this.stepSize = params.gaussianInitialOneStepSize;
            else
                this.stepSize = params.cauchyInitialOneStepSize;
        }
        else if (params.mutationStrategy == MutationStrategy.N_STEP) {
            this.stepSizes = new double[10];
            for (int i = 0; i < this.stepSizes.length; i++) {

                if (params.mutationDistribution == MutationDistribution.GAUSSIAN)
                    this.stepSizes[i] = params.gaussianInitialNStepSize;
                else
                    this.stepSizes[i] = params.cauchyInitialNStepSize;
            }
        }
    }

    public Genotype(double[] genes, double[] stepSizes) {
        this.genes = genes;
        this.stepSizes = stepSizes;
    }

    public Genotype(double[] genes, double stepSize) {
        this.genes = genes;
        this.stepSize = stepSize;
    }

//    public Genotype(double[] genes, double fitness) {
//        this.genes = genes;
//        this.fitness = fitness;
//    }

    /**
     * Compares to another genotype by its fitness.
     */
    @Override
    public int compareTo(Genotype genotype) {
        return Double.compare(this.fitness, genotype.fitness);
    }
}